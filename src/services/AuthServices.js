import Api from './Api'

export default {

  register (credentials) {

    return Api().post('register', credentials)
  },
  getUserDetails (credentials){

    return Api().post('login',credentials)
  }
}
