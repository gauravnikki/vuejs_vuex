import Api from './Api'

export default {

  postAddProduct (prductObj) {

    return Api().post('addProduct', prductObj)
  },

  getProducts () {

    return Api().get('getProducts')
  }
}
