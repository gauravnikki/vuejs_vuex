


const state = {
  essentialLinks: [

    {
      title: "Shop",
      icon: "shop",
      link: "/shopPage",
    },
    {
      title: "Add to shop",
      icon: "settings",
      link: "/addproduct",
    },
    {
      title: "Cart",
      icon: "fa-solid fa-cart-shopping",
      link: "/cartPage",
    },
    {
      title: "Checkout",
      icon: "settings",
      link: "/checkout",
    }
  ],
  }
const mutations = {
  //addProduct is the mutaion created in action which will set the value of state object..
  upDateLinks (state,payload) {
// send to db
      // console.log('payload:', payload)
      payload.forEach(element => {
        // console.log(element)
        state.essentialLinks.push(element)
      });
    },
}
const actions = {

  async upDateLinks({commit},payload) {
    console.log(payload[3])
    commit('upDateLinks', payload)
  }
}
const getters = {
  links: (state) => {

    return state.essentialLinks
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

