const state = {
  cartProducts:[

  ]
  }
const mutations = {
  //addProduct is the mutaion created in action which will set the value of state object..
  addToCart (state,payload) {
      // send to db
      console.log('payload:', payload)
      state.cartProducts.push(payload)
    },

  removeFromCart (state,index) {
    // send to db
    console.log('payload:', index)
    state.cartProducts.splice(index,1)
  }
}
const actions = {
  updateCart({commit}, payload) {
    // addProduct is mutation here..
    // console.log('payload:', payload)
    commit('addToCart',payload)
  },


  removeFromCart({commit}, index) {
    // addProduct is mutation here..
    // console.log('payload:', payload)
    commit('removeFromCart',index)
  }
}
const getters = {
  cartProducts: (state) => {
    return state.cartProducts
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

