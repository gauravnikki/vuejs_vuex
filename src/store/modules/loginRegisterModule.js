
import AuthServices from "src/services/AuthServices";
import createPersistedState from "vuex-persistedstate"
import { LocalStorage } from 'quasar'

const state = {
  user: {
    id: null,
    email: null,
    is_superuser: false
  },

  token: null,

  loginError:{

    errMsg: '',
    iserror: false

  },

  isLoggedIn: false


  }

const mutations = {
  //addProduct is the mutaion created in action which will set the value of state object..
  setUser (state,payload) {

      // state.user.id = payload.user.id,
      // state.user.email = payload.user.email,
      // state.user.is_superuser = payload.user.is_superuser,
      // state.user.id = payload.user.id,
      Object.assign(state.user,payload.user)
      state.token = payload.token,
      state.isLoggedIn = payload.isLoggedIn
    },

  loginError(state,payload) {

    state.loginError.errMsg = payload.errMsg
    state.loginError.iserror = payload.iserror
  },


  logOut(state,payload) {

    state.isLoggedIn = payload.isLoggedIn,
    state.user = payload.clearLoginData
  }
}
const actions = {
  async login({commit}, payload) {
    // addProduct is mutation here..
    // console.log('payload:', payload)

    const response = await AuthServices.getUserDetails(payload)

    if(response.data.iserror){

      commit('loginError',{errMsg:response.data.error, iserror: response.data.iserror})

    }else {
      console.log(response.data)

      LocalStorage.set('token', payload.token)
      commit('setUser',{
        user:{
          id:response.data.login.id,
          email: response.data.login.email,
          is_superuser:response.data.login.is_superuser
          },
        token: response.data.token,
        isLoggedIn: true
        })
    }
  },
  isLogOut({commit}, payload) {

    commit('logOut',{isLoggedIn: payload.isLogOut,clearLoginData:{}})
  }
}
const getters = {
  user: (state) => {
    return state.user
  },

  token: (state) => {
    return state.token
  },

  loginError: (state) => {

    return state.loginError
  },
  isLoggedIn: (state) => {

    return state.isLoggedIn
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

