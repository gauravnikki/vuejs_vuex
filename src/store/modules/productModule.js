import ProductServices from "src/services/ProductServices";


const state = {
  products:[
    // {
    //   id:1,
    //   title: "first book",
    //   src: "https://cdn.quasar.dev/img/parallax2.jpg",
    //   description:"this book is awesome"
    // },
    // {
    //   id:2,
    //   title: "second book",
    //   src: "https://picsum.photos/200/300",
    //   description:"this book is awesome"
    // },
    // {
    //   id:3,
    //   title: "third book",
    //   src: "https://picsum.photos/seed/picsum/200/300",
    //   description:"you must read this book"
    // }
  ]
  }
const mutations = {
  //addProduct is the mutaion created in action which will set the value of state object..
    addProduct (state,postData) {
// send to db
      // console.log('payload:', payload)
      state.products.push(postData)
    },
    setProducts (state, products) {

      let objects = {
        title: '',
        src: '',
        price: 0,
        description: ''
      }
      if(products.status === 200) {

        products.data.forEach(product => {
          objects.title = product.title.toUpperCase()
          objects.src = product.src
          objects.price = product.price
          objects.description = product.description
          state.products.push(objects)
          objects = {}
        });
      }
    }
}
const actions = {
  async updateShop({commit}, payload) {

    let postData = {
      title: payload.title,
      src: payload.src,
      price: parseFloat(payload.price),
      description: payload.description,
      userId: payload.userId
    }

    const response = await ProductServices.postAddProduct({postData})
      if (response.status === 200){

        this.postData = {}
        this.postData.title = response.data.title
        this.postData.src = response.data.src
        this.postData.price = response.data.price
        this.postData.description = response.data.description
        commit('addProduct',postData)
        this.$router.push('/shopPage')
      }else{

        console.log("Error during retriving data")
      }
  },
  async product({commit}) {

    const products = await ProductServices.getProducts()
    commit('setProducts', products)
    // console.log(products)
  }
}
const getters = {
  products: (state) => {

    return state.products
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

