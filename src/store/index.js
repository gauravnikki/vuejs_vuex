import { store } from 'quasar/wrappers'
import { createStore } from 'vuex'
import state from './module-example/state'

// import example from './module-example'
import productsModule from './modules/productModule'
import cartModule from './modules/cartModule'
import loginModule from './modules/loginRegisterModule'
import linkModule from './modules/linkModule'

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default store(function (/* { ssrContext } */) {
  const Store = createStore({
    modules: {
      productsModule,
      cartModule,
      loginModule,
      linkModule
    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: process.env.DEBUGGING
  })

  return Store
})

