
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/shopPage', component: () => import('src/pages/shopPage.vue') },
      { path: '/addproduct', component: () => import('src/pages/addProduct.vue') },
      { path: '/cartPage', component: () => import('src/pages/cartPage.vue') },
      { path: '/register', component: () => import('src/pages/register.vue') },
      { path: '/login', component: () => import('src/pages/login.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
